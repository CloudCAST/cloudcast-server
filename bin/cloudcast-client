#!/usr/bin/env perl

use warnings;
use strict;

use FindBin;
BEGIN { unshift @INC, "$FindBin::Bin/../lib" }

use CloudCAST;
use CloudCAST::Codes -cloudcast;

use Mojolicious::Lite;
use Class::Load 'try_load_class';
use URI;
use URI::QueryParam;
use Getopt::Long::Descriptive;
use Path::Tiny qw( cwd path );
use File::Share qw( dist_file );
use Log::Any qw( $log );
use JSON::MaybeXS;

use Log::Any::Adapter;
Log::Any::Adapter->set('Stderr');

my ($options, $usage);

BEGIN {
  my $width;
  if (try_load_class 'Term::ReadKey') {
    my @ret = Term::ReadKey::GetTerminalSize();
    $width = shift(@ret) - 1;
  }
  else {
    $width = 79;
  }

  ($options, $usage) = describe_options(
    "%c %o file\n\nAlternative environment variables are shown in parentheses\n",
    [ 'decoder=s',
        q{String ID of decoder to use},
        { default => 'voxforge' } ],
    [ 'username=s',
        q{Name of user account},
        { default => 'foo' } ],
    [ 'application=s',
        q{Name of application},
        { default => 'bar' } ],
    [ 'host=s',
        q{Base URL of host},
        { default => 'cloudcast.sheffield.ac.uk' } ],
    [ 'endpoint=s',
        q{URL to connect to for registration},
        { default => 'api/v0/user/:username/app/:application/asr/decode' } ],
    [ 'protocol=s',
        q{Protocol to use for registration},
        { default => 'wss' } ],
    [ 'url=s',
        q{URL to connect to for registration},
        { default => ':protocol://:host/:endpoint' } ],
    [ 'byterate=s',
        q{URL to connect to for registration},
        { default => '32000' } ],
    [ 'screen-width=s',
        q{Number of columns in terminal},
        { default =>  $width } ],
    [ 'format', q{Specify output format (one of 'json' or 'text')},
        { default =>  'text', one_of => [
          [ json => '', { hidden => 1 } ],
          [ text => '', { hidden => 1 } ],
        ] } ],
    [],
    [ 'help',
        q{Print usage message and exit},
        { shortcircuit => 1 } ],
    { show_defaults => 1 },
  );

  print($usage->text) and exit if $options->help;

  $options->{endpoint} =~ s/:(\w+)/$options->{$1}/g;
  $options->{url}      =~ s/:(\w+)/$options->{$1}/g;

  $options->{url} = URI->new( $options->{url} );
  $options->{url}->host( delete $options->{host} );
  $options->{url}->scheme( delete $options->{protocol} );
  $options->{url}->path( delete $options->{endpoint} );

  delete $options->{username};
  delete $options->{application};

}

my $json = ($options->format eq 'json')
  ? JSON::MaybeXS->new( utf8 => 1, pretty => 1 ) : '';

$options->url->query_param(decoder => $options->decoder);

my $infile = shift or die "Usage: $0 [options] file\n";
my $size = -s $infile;
my $per_second = 4;

my $ua = Mojo::UserAgent->new( inactivity_timeout => 0 );

$log->infof('Client created, connecting to server at %s', $options->url);
$log->infof(q{Requesting '%s' decoder}, $options->decoder);

$ua->websocket($options->url->as_string => sub {
  my ($ua, $tx) = @_;
  unless ($tx->is_websocket) {
    $log->warnf(
      'WebSocket handshake failed! Did not connect to %s', $options->url
    );
    return;
  }

  $tx->on(json => sub {
    my ($tx, $msg) = @_;

    if ($msg->{status} == CLOUDCAST_SUCCESS) {

      if ($json) { print $json->encode($msg) }
      else       { process($msg) }
    }

    else {
      $log->errorf('Received error from server (status %s)', $msg->{status});
      $log->warnf('Error message: %s', $msg->{message}) if defined $msg->{message};
    }
  });

  $tx->on(finish => sub {
    my ($tx, $code, $reason) = @_;
    $log->infof('WebSocket closed with status %s', $code);
    $log->debugf('Reason: "%s"', (defined $reason and $reason ne '') ? $reason : 'None given');
  });

  $log->info('Sending audio');

  open ( my $fh, $infile )
    or die "$infile: $!\n";

  my $timer;
  $timer = Mojo::IOLoop->recurring( 1 / $per_second => sub {

    if ($tx->is_finished) {
      $log->debugf('Could not finish sending data: transaction is closed');
      Mojo::IOLoop->remove($timer);
    }
    else {
      my $buffer;
      my $continue = read $fh, $buffer, $options->byterate / $per_second;
      if ($continue) {
        use bytes ();
        $log->debugf('Sending buffer of size %i to server', bytes::length($buffer));
        $tx->send({ binary => $buffer });
      }
      else {
        $log->info('Audio sent, now sending EOS');
        $tx->send('EOS');
        Mojo::IOLoop->remove($timer);
      }
    }
  });

});

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;

sub process {
  my $msg = shift;

  my $line_prefix = '...';
  my $transcript = $msg->{result}->{hypotheses}->[0]->{transcript};
  $transcript =~ s%\n%\\n%g if defined $transcript;
  if ($transcript) {
    local $| = 1;
    my $line;
    if (length($transcript) >= $options->screen_width) {
      my $msgt = length($line_prefix) - $options->screen_width;
      $line = sprintf("${line_prefix}%s", substr($transcript, $msgt));
    }
    else {
      $line = $transcript
    }
    print "\r", $line;
  }

  if ($msg->{result}->{final}) {
    print "\r$transcript\n";
  }
}
