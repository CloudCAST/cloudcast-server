use warnings;
use strict;

package Mojolicious::Plugin::PageHeaders;

use Mojo::Base 'Mojolicious::Plugin';

sub register {
    my ($self, $app, $args) = @_;

    $args ||= {};
    my $count_key     = $args->{stash_key}     // 'element_count';
    my $page_key      = $args->{page_key}      // 'page';
    my $per_page_key  = $args->{per_page_key}  // 'per_page';
    my $first_page    = $args->{first_page}    // 1;
    my $url_extractor = $args->{url_extractor} // sub {
      my ($c) = @_;
      return $c->req->url->clone;
    };

    my $header_maker = sub {
      my ($url, $param) = @_;
      $url = Mojo::URL->new($url) unless ref $url;
      $url->query(
        # Use an array reference to merge into query
        [ per_page => $param->{per_page}, page => $param->{page} ],
      );
      return sprintf '%s;rel=%s', $url->to_string, $param->{rel};
    };

    $app->hook( after_render => sub {
        my $c = shift;

        my $url = $url_extractor->( $c );
        my $count = $c->stash( $count_key )
          // $c->validation->param( $count_key );

        return unless $count;

        my $page = $c->stash( $page_key )
          // $c->validation->param( $page_key );

        return unless $page;

        my $per_page = $c->stash( $per_page_key )
          // $c->validation->param( $per_page_key );

        return unless $per_page;

        my @link;

        push @link, $header_maker->( $url => {
          rel => 'first',
          per_page => $per_page,
          page => $first_page,
        });

        my $last_page = 1 + int $count / $per_page;

        if ($first_page < $page) {
          push @link, $header_maker->( $url => {
            rel => 'prev',
            per_page => $per_page,
            page => $page - 1,
          });
        }

        if ($last_page >= $page + 1) {
          push @link, $header_maker->( $url => {
            rel => 'next',
            per_page => $per_page,
            page => $page + 1,
          });
        }

        push @link, $header_maker->( $url => {
          rel => 'last',
          per_page => $per_page,
          page => $last_page
        });

        $c->res->headers->link( join ',', @link );
    });
}

1;

__END__

=encoding UTF-8

=head1 NAME

Mojolicious::Plugin::PageHeaders - Add relation links to headers

=head1 SYNOPSIS

    $self->plugin( PageHeaders => {
        stash_key     => 'element_count', # Total number of elements
        page_key      => 'page',          # Key for 'current page number' value
        per_page_key  => 'per_page',      # Key for 'entries per page' value
        first_page    => 1,               # Zero or one based?
        url_extractor => sub { ... },     # Extract the URL from the controller
    });

=head1 PARAMETERS

=over 4

=item B<stash_key>

=item B<page_key>

=item B<per_page_key>

=item B<first_page>

=item B<url_extractor>

=back

=head1 AUTHOR

=over 4

=item José Joaquín Atria, C<< <jjatria@cpan.org> >>

=back

=head1 LICENSE AND COPYRIGHT

Copyright 2017 University of Sheffield

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

=cut
