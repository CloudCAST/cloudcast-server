package CloudCAST::Types;

use strict;
use warnings;

our $VERSION = '0';

use Type::Library -base;

use Type::Utils -all;
use Types::Standard qw( HashRef Undef );
use Class::Load qw( load_class );

foreach my $type (qw( Worker Decoder )) {
  my $class = 'CloudCAST::' . $type;

  class_type $type, { class => $class };

  coerce $type,
    from HashRef,
      via {
        load_class $class;
        $class->new( $_ );
      };
    from Undef,
      via {
        load_class $class;
        $class->new;
      };
}

1;
