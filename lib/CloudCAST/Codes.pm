package CloudCAST::Codes;

our $VERSION = '0';

my (%ws, %cc, %all);

BEGIN {
  %ws = (
    WS_FINISHED                => 1000,
    WS_GOING_AWAY              => 1001,
    WS_PROTOCOL_ERROR          => 1002,
    WS_UNACCEPTABLE_DATA       => 1003,
    WS_BAD_INPUT               => 1007,
    WS_POLICY_VIOLATION        => 1008,
    WS_TOO_BIG                 => 1009,
    WS_BAD_EXTENSION           => 1010,
    WS_UNEXPECTED_CONDITION    => 1011,
  );

  %cc = (
    CLOUDCAST_SUCCESS          => 4000,
    CLOUDCAST_ABORTED          => 4001,
    CLOUDCAST_NO_INPUT         => 4002,
    CLOUDCAST_BAD_INPUT        => 4003,
    CLOUDCAST_BAD_PERMISSIONS  => 4004,
    CLOUDCAST_NOT_AVAILABLE    => 4005,
    CLOUDCAST_RESOURCE_MISSING => 4006,
    CLOUDCAST_NOT_SUPPORTED    => 4007,
    CLOUDCAST_BAD_GRAMMAR      => 4008,
    CLOUDCAST_DECODER_ERROR    => 4009,
    CLOUDCAST_CREATED          => 4100,
    CLOUDCAST_CONNECTED        => 4101,
    CLOUDCAST_INITIALIZED      => 4102,
    CLOUDCAST_READY            => 4103,
    CLOUDCAST_STARTED          => 4104,
    CLOUDCAST_FINISHING        => 4105,
    CLOUDCAST_ABORTING         => 4106,
    CLOUDCAST_FINISHED         => 4199,
  );

  %all = ( %ws, %cc );
}

use constant \%all;
use Exporter::Shiny keys(%all);

our %EXPORT_TAGS = (
  all       => [ keys %all ],
  websocket => [ keys %ws  ],
  cloudcast => [ keys %cc  ],
);

1;


__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Codes - Response codes understood by the CloudCAST server

=head1 DESCRIPTION

=head1 CODES

=head2 WebSocket response codes

These response codes are the same as those specified in
L<RFC 6455|https://tools.ietf.org/html/rfc6455>. Descriptions of those codes
are reproduced below for convenience.

=over 4

=item B<1000>

1000 indicates a normal closure, meaning that the purpose for which the
connection was established has been fulfilled.

This is exported as C<WS_FINISHED>.

=item B<1001>

1001 indicates that an endpoint is "going away", such as a server going down
or a browser having navigated away from a page.

This is exported as C<WS_GOING_AWAY>.

=item B<1002>

1002 indicates that an endpoint is terminating the connection due to a
protocol error.

This is exported as C<WS_PROTOCOL_ERROR>.

=item B<1003>

1003 indicates that an endpoint is terminating the connection because it has
received a type of data it cannot accept (e.g., an endpoint that understands
only text data MAY send this if it receives a binary message).

This is exported as C<WS_UNACCEPTABLE_DATA>.

=item B<1007>

1007 indicates that an endpoint is terminating the connection because it has
received data within a message that was not consistent with the type of the
message (e.g., non-UTF-8 [L<RFC 3629|https://tools.ietf.org/html/rfc3629>]
data within a text message).

This is exported as C<WS_BAD_INPUT>.

=item B<1008>

1008 indicates that an endpoint is terminating the connection because it has
received a message that violates its policy.  This is a generic status code
that can be returned when there is no other more suitable status code (e.g.,
1003 or 1009) or if there is a need to hide specific details about the policy.

This is exported as C<WS_POLICY_VIOLATION>.

=item B<1009>

1009 indicates that an endpoint is terminating the connection because it has
received a message that is too big for it to process.

This is exported as C<WS_TOO_BIG>.

=item B<1010>

1010 indicates that an endpoint (client) is terminating the connection because
it has expected the server to negotiate one or more extension, but the server
didn't return them in the response message of the WebSocket handshake. The list
of extensions that are needed SHOULD appear in the /reason/ part of the Close
frame. Note that this status code is not used by the server, because it can
fail the WebSocket handshake instead.

This is exported as C<WS_BAD_EXTENSION>.

=item B<1011>

1011 indicates that a server is terminating the connection because it
encountered an unexpected condition that prevented it from fulfilling the
request.

This is exported as C<WS_UNEXPECTED_CONDITION>.

=back

=head2 CloudCAST codes

The L<CloudCAST> server uses the status codes in the 4000-4099 range for its
custom response codes sent over WebSocket connections, in order not to collide
with others specified in the protocol.

=over 4

=item B<4000>

4000 indicates a transaction that was completed succesfully.

This is exported as C<CLOUDCAST_SUCCESS>.

=item B<4001>

4001 indicates a transaction that was aborted unexpectedly.

This is exported as C<CLOUDCAST_ABORTED>.

=item B<4002>

4002 indicates that no input was received through the connection, even though
the server expected some.

This is exported as C<CLOUDCAST_NO_INPUT>.

=item B<4003>

4003 indicates that the input received by the server through the connection was
not apropriate for the requested action.

This is exported as C<CLOUDCAST_BAD_INPUT>.

=item B<4004>

4004 indicates that the request to the server was forbidden because of missing
credentials.

This is exported as C<CLOUDCAST_BAD_PERMISSIONS>.

=item B<4005>

4005 indicates that the requested service is not available. Most commonly, this
will be due to there not being any workers available to process the transaction.

This is exported as C<CLOUDCAST_NOT_AVAILABLE>.

=item B<4006>

4006 indicates that a resource that was expected to be present for the
processing of the request has gone unexpectedly and unrecoverably missing.

This is exported as C<CLOUDCAST_RESOURCE_MISSING>.

=item B<4007>

4007 indicates that the server received a request to perform an unsupported
action.

This is exported as C<CLOUDCAST_NOT_SUPPORTED>.

=item B<4008>

4008 indicates that the grammar used for the decoding had an error.

This is exported as C<CLOUDCAST_BAD_GRAMMAR>.

=item B<4009>

This is exported as C<CLOUDCAST_DECODER_ERROR>.

=back

=head2 CloudCAST states

The 4100-4199 range is used by L<CloudCAST> to report internal states.

=over 4

=item B<4100>

4100 indicates a resource has been created. The resource might not be in a
usable state, but it should have access to all its internal methods.

This is exported as C<CLOUDCAST_CREATED>.

=item B<4101>

4101 indicates that the created resource has been connected. This state is not
suitable for all objects.

This is exported as C<CLOUDCAST_CONNECTED>.

=item B<4102>

4102 indicates that the resource has finished whatever initialisation process
it requires. The resource might still not be ready to e used at this point.

This is exported as C<CLOUDCAST_INITIALIZED>.

=item B<4103>

4103 indicates that the resource is ready to process incoming connections or
requests.

This is exported as C<CLOUDCAST_READY>.

=item B<4104>

4104 indicates that the resource has started processing a request.

This is exported as C<CLOUDCAST_STARTED>.

=item B<4105>

4105 indicates that the resource has received a signal to stop processing, and
is in the process of doing so (but not yet finished). For example, a decoder
resource has received an end-of-stream signal, and is making sure no remaining
results are left in the pipeline waiting to be sent.

This is exported as C<CLOUDCAST_FINISHING>.

=item B<4106>

4106 indicates that the resource has received a signal to abort a request
immediately. Once the resource has received this signal, it will inevitably
result in the process being aborted (resulting in a 4001 response).

This is exported as C<CLOUDCAST_ABORTING>.

=item B<4199>

4199 indicates that the resource is completely finished processing a request.
This code specifies nothing regarding the result of the request in question.

This is exported as C<CLOUDCAST_FINISHED>.

=back

=head1 SEE ALSO

=over 4

=item L<CloudCAST>

=back

=head1 ACKNOWLEDGEMENTS

Work on this server has been funded by the Leverhulme Trust as part of the
CloudCAST project.

This server is heavily based in the Kaldi GStreamer server and other work by
Tanel Alumae.

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016-2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
