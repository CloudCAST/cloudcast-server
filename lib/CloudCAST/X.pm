package CloudCAST::X;

use Moose;

with 'Throwable::X';
use Throwable::X -all;

has '+ident' => ( default => 'error' );

has '+message_fmt' => ( default => 'An error occured: %{ident}s' );

has http_status => (
  is      => 'ro',
  default => 400,
  traits  => [ Payload ],
  lazy => 1,
);

1;
