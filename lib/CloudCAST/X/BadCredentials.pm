package CloudCAST::X::BadCredentials;

use Moose;

extends 'CloudCAST::X::Forbidden';
use Throwable::X -all;
use Types::Standard qw( Str );

has '+ident' => ( default => sub { 'wrong_credentials' } );

has '+message_fmt' => (
  default => 'Could not authenticate with the provided credentials',
);

has '+action' => ( default => 'authentication' );

1;
