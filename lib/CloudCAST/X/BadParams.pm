package CloudCAST::X::BadParams;

use Moose;

extends 'CloudCAST::X';
use Throwable::X -all;
use Types::Standard qw( Str );

has '+ident' => ( default => sub { 'bad_params' } );

has '+message_fmt' => (
  default => '%{function}s was called with the wrong set of parameters',
);

has function => (
  is => 'ro',
  isa => Str,
  required => 1,
  traits => [ Payload ],
);

has params => (
  is => 'ro',
  traits => [ Payload ],
);

1;
