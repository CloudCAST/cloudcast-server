package CloudCAST::X::AlreadyExists;

use Moose;

extends 'CloudCAST::X';
use Throwable::X -all;
use Types::Standard qw( Str HashRef );
use String::Errf qw(errf);

has model => (
  is => 'ro',
  isa => Str,
  traits => [ Payload ],
  required => 1,
);

has query => (
  is => 'ro',
  isa => HashRef,
  traits => [ Payload ],
);

has '+message_fmt' => (
  default => 'A %{model}s by that name already exists',
);

sub BUILDARGS {
  my ($self, $args) = @_;
  $args->{ident} //= $args->{model} . '_already_exists';
  return $args;
}

1;
