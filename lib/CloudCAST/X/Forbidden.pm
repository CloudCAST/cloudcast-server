package CloudCAST::X::Forbidden;

use Moose;

extends 'CloudCAST::X';
use Throwable::X -all;
use Types::Standard qw( Str );

has '+ident' => ( default => sub { 'forbidden' } );

has '+message_fmt' => (
  default => 'Current user does not have permissions to perform this action',
);

has '+http_status' => ( default => 403, );

has action => (
  is => 'ro',
  isa => Str,
  required => 1,
  traits => [ Payload ],
);

1;
