package CloudCAST::X::NotFound;

use Moose;

extends 'CloudCAST::X';
use Throwable::X -all;
use Types::Standard qw( Str HashRef );

has '+message_fmt' => (
  default => 'Could not find the requested %{model}s',
);

has '+http_status' => ( default => 404 );

has model => (
  is => 'ro',
  isa => Str,
  traits => [ Payload ],
  required => 1,
);

has query => (
  is => 'ro',
  isa => HashRef,
  traits => [ Payload ],
);

sub BUILDARGS {
  my ($self, $args) = @_;
  $args->{ident} //= $args->{model} . '_not_found';
  return $args;
}

1;
