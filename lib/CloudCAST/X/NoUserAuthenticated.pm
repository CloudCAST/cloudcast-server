package CloudCAST::X::NoUserAuthenticated;

use Moose;

extends 'CloudCAST::X::Forbidden';
use Throwable::X -all;
use Types::Standard qw( Str );

has '+ident' => ( default => sub { 'no_user_authenticated' } );

has '+message_fmt' => (
  default =>
      'An action that requires an authenticated user was requested, '
    . 'but no user is authenticated',
);

1;
