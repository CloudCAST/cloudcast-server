package CloudCAST::Base;

our $VERSION = '0';

use Moo::Role;
use CloudCAST::Codes -cloudcast;
use UUID qw();

use Log::Any;
my $log = Log::Any->get_logger(category => 'CloudCAST');

has id => (
  is => 'rw',
  default => sub { UUID::uuid() },
);

has state => (
  is => 'rw',
  default => CLOUDCAST_CREATED,
  trigger => sub {
    $log->debugf('%s state changed to %s', ref $_[0], $_[0]->state)
  },
);

1;
