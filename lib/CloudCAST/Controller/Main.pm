package CloudCAST::Controller::Main;

our $VERSION = '0';

use Moo;

extends 'Mojolicious::Controller';

use Log::Any;
my $log = Log::Any->get_logger(category => 'CloudCAST');

sub list_users {
  my $self = shift->openapi->valid_input or return;

  my $page     = $self->validation->param('page');
  my $per_page = $self->validation->param('per_page');

  $self->stash( element_count => $self->resultset('User')->count );

  my $users = $self->resultset('User')->search_rs(
    undef,
    { page => $page, rows => $per_page }
  );

  return $self->render(openapi => [ map { $_->render } $users->all ]);
}

1;
