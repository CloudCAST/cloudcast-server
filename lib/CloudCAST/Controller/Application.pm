package CloudCAST::Controller::Application;

our $VERSION = '0';

use Moo;
use uni::perl;

extends 'Mojolicious::Controller';

use Log::Any;
my $log = Log::Any->get_logger(category => 'CloudCAST');

sub read {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;
  return $self->render( openapi => $self->stash('application')->render );
}

sub create {
  my $self = shift->openapi->valid_input or return;

  my $uid = $self->validation->param('uid');
  my $user = $self->load( User => $uid );

  my $app = $self->validation->param('application');

  my $current = $self->current_user;
  # Only allow if current user is root or the namespace user
  return $self->reply->forbidden
    unless $current->id eq 1 or $current->id eq $user->id;

  $self->does_not_exist( Application => {
    owner => $user->id,
    name  => $app->{name},
  });

  # Calling new_result provides additional default values (like default
  # random passwords), but then we need to manually insert it to the db
  my $application = $self->resultset('Application')
    ->new_result({ %{$app}, owner => $user->id })->insert;

  return $self->render( status => 201, openapi => $application->render );
}

sub update {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;
  $self->stash('application')->update($self->validation->param('application'));
  return $self->render( openapi => $self->stash('application')->render );
}

sub delete {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;
  $self->stash('application')->delete;
  return $self->render( status => 204, openapi => undef );
}

sub clone {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;

  my $source = $self->stash('application');
  my $user = $self->current_user;
  my $input = $self->validation->param('application') // {};

  $self->does_not_exist( application => {
    owner => $user->id,
    name => ($input->{name} // $source->name),
  });

  my $copy = $source->copy({
    %{$input},
    owner     => $user->id,
    parent    => $source->id,
  });
  $copy->create_related(
    application_users => { member => $user->id, role => 'owner' },
  );

  return $self->render( status => 201, openapi => $copy->render );
}

sub create_key {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;
  my $key = $self->current_user->generate_key(
    $self->stash('application')->id,
  );
  return $self->render( status => 201, openapi => $key->render );
}

sub delete_key {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;
  $self->current_user->expire_keys( $self->stash('application')->id );
  return $self->render( status => 204, openapi => undef );
}

sub send_invite {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;

  my $invite = $self->validation->param('invite');
  my $target = $self->load( User => $invite->{name} );
  my $uid = $self->stash('user');
  my $aid = $self->stash('application');

  my $user = $self->current_user;

  my $is_allowed = 'can_write_' . $invite->{role};
  my ($perms) = $user->applications( $uid->name . '/' . $aid->name );

  return $self->reply->forbidden unless $perms->role->$is_allowed;

  my $jwt = $self->jwt->encode({
    'member' => $target->name,
    'role' => $invite->{role},
    exp => DateTime->now->add( days => 1 )->epoch,
  });

  my $url = $self->url_for->to_abs->query( token => $jwt );

  $self->minion->enqueue( email_task => [
    $target->email_address,
    'Confirm registration',
    "Please visit $url to confirm",
  ]);

  return $self->render( status => 202, openapi => undef );
}

sub claim_invite {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;

  my $jwt = $self->jwt->decode( $self->validation->param('token') );
  my $app = $self->stash('application');
  my $user = $self->load( User => $jwt->{member} );

  $app->create_related( application_users => {
    member => $user->id,
    role   => $jwt->{role},
  });

  return $self->render( status => 201, openapi => $app->render );
}

sub delete_member {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;

  my $uid = $self->stash('user');
  my $aid = $self->stash('application');
  my $target = $self->load( User => $self->validation->param('mid') );

  my $user = $self->current_user;

  my ($role) = $target->applications( $uid->name . '/' . $aid->name );

  # If no role was found, then this user is not a member
  # Since this method is idempotent, skip
  if ($role) {
    my ($perms) = $user->applications( $uid->name . '/' . $aid->name );
    my $is_allowed = 'can_write_' . $role->role->name;

    return $self->reply->forbidden unless $perms->role->$is_allowed;

    $self->stash('application')
      ->delete_related(
        application_users => { member => $target->id }
      );
  }

  return $self->render( status => 204, openapi => undef );
}

sub list_decoders {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;

  my $app = $self->stash('application');
  $self->stash( element_count => scalar($app->decodings) );
  return $self->render(openapi => [ map { $_->render } $app->decodings ]);
}

sub read_default_decoder {
  my $self = shift->openapi->valid_input or return;
  $self->load_stash;

  my $decoding = $self->stash('application')->default_decoding;
  return $self->render( json => ( $decoding ? $decoding->render : undef ) );
}

sub load_stash {
  my ($self) = @_;

  my $app = $self->app;
  my $user_name = $self->validation->param('uid');
  my $app_name  = $self->validation->param('aid');

  my $user        = $self->load( User => $user_name );
  my $application = $self->load( Application => {
    namespace => $user->name,
    name      => $app_name,
  });

  $self->stash( user        => $user );
  $self->stash( application => $application );

  return $self;
}

1;
