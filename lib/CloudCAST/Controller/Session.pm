package CloudCAST::Controller::Session;

our $VERSION = '0';

use Moo;

with 'CloudCAST::Base';
extends 'Mojolicious::Controller';

use Log::Any;
my $log = Log::Any->get_logger(category => 'CloudCAST');

sub login {
  my $self = shift->openapi->valid_input or return;

  my $credentials = $self->validation->param('credentials');

  if ($self->authenticate( @{$credentials}{qw( name password )} )) {
    my $token = $self->jwt->encode({
      exp => DateTime->now->add( hours => 1 )->epoch,
      sub => $self->current_user->id
    });

    $self->res->headers( Authorization => "Bearer $token" );
    return $self->render( openapi => { token => $token });
  }

  return $self->throw( 'BadCredentials' );
}

1;
