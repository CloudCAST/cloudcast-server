package CloudCAST::Controller::User;

our $VERSION = '0';

use uni::perl;

use Moo;

extends 'Mojolicious::Controller';

use Log::Any;
use Try::Tiny;
use Mojo::JWT;

my $log = Log::Any->get_logger(category => 'CloudCAST');

sub auth {
  my $self = shift->openapi->valid_input or return;

  return 1 unless $self->req->headers->authorization;

  if ($self->req->headers->authorization =~ /^hmac/) {
    $self->hmac_authentication;
  }
  elsif ($self->req->headers->authorization =~ /^Bearer/) {
    $self->jwt_authentication;
  }

  $log->debugf('User %s is authenticated', $self->current_user->name)
    if $self->is_user_authenticated;

  # Only some API calls require authentication,
  # so we return true to pass the call down.
  # Proper authentication checks are done in the specific actions
  return 1;
}

sub list_applications {
  my $self = shift->openapi->valid_input or return;
  my $user = $self->load( User => $self->validation->param('uid') );
  $self->stash( element_count => scalar($user->applications) );
  return $self->render(openapi => [
    map { $_->application->render } $user->applications
  ]);
}

sub create {
  my $self = shift->openapi->valid_input or return;

  my $user = $self->validation->param('user');
  $self->does_not_exist( User => $user->{name} );

  # Calling new_result provides additional default values (like default
  # random passwords), but then we need to manually insert it to the db
  $user = $self->resultset('User')
    ->new_result( $user )->insert;

  return $self->render( status => 201, openapi => $user->render, );
}

sub read {
  my $self = shift->openapi->valid_input or return;

  my $uid = $self->validation->param('uid');

  my $user;
  if (defined $uid) {
    $user = $self->load( User => $uid );
  }
  else {
    $user = $self->current_user
      or $self->throw( 'NoUserAuthenticated' );
  }

  return $self->render( openapi => $user->render );
}

sub update {
  my $self = shift->openapi->valid_input or return;

  my $uid = $self->validation->param('uid');

  my $user;
  if (defined $uid) {
    $user = $self->load( User => $uid );
  }
  else {
    $user = $self->current_user
      or $self->throw( 'NoUserAuthenticated' );
  }

  $user->update($self->validation->param('user'));
  return $self->render( openapi => $user->render );
}

sub delete {
  my $self = shift->openapi->valid_input or return;
  my $user = $self->current_user;
  my $uid = $self->validation->param('uid');

  return $self->reply->forbidden
    unless $uid eq $user->name or $user->is_admin;

  $self->load( User => $uid )->delete;

  return $self->render( status => 204, openapi => undef );
}

sub create_key {
  my $self = shift->openapi->valid_input or return;
  my $user = $self->current_user;
  my $uid = $self->validation->param('uid');

  return $self->reply->forbidden
    unless $uid eq $user->name or $user->is_admin;

  my $key = $self->load( User => $uid )->generate_key;
  return $self->render( status => 201, openapi => $key->render );
}

sub delete_key {
  my $self = shift->openapi->valid_input or return;
  my $user = $self->current_user;
  my $uid = $self->validation->param('uid');

  return $self->reply->forbidden
    unless $uid eq $user->name or $user->is_admin;

  $self->load( User => $uid )->expire_keys;
  return $self->render( status => 204, openapi => undef );
}

sub jwt_authentication {
  my ($self) = @_;
  my ($token) = reverse split ' ', $self->req->headers->authorization;

  try {
    my $jwt = $self->jwt->decode($token);
    $self->authenticate( undef, undef, { auto_validate => $jwt->{sub} } );
  }
  catch {
    $log->warn( $_ );
  };

  return;
}

sub hmac_authentication {
  my ($self) = @_;

  my ($auth) = reverse split ' ', $self->req->headers->authorization;
  my ($user_name, $digest) = split ':', $auth;

  return 1 unless defined $user_name;

  my $url = $self->req->url->clone;
  my $query = $self->req->url->query->to_hash;

  $url->query({});
  $url->query([ $_ => $query->{$_} ]) foreach sort keys %{$query};

  my $date = $self->req->headers->date;
  my $query_string = $self->req->method . '+' . $url->to_string . '+' . $date;
  my $user = try { $self->load( User => $user_name ) };

  return 1 unless defined $user;

  # If this is an application endpoint, try to authenticate with
  # an app-specific API key (if available)
  my $app_name = $1 if $self->req->url->to_string =~ m{/apps?/([a-zA-Z0-9-]+)\b};

  if ($app_name) {
    try {
      my $app = $self->load( Application => {
        owner => $user->id,
        name  => $app_name
      });
      my ($key) = $user->keys( $app->id );

      if ($key) {
        my $hash = $self->app->hmac_b64('SHA256', $key->key, $query_string);
        $self->authenticate(undef, undef, { auto_validate => $user->id } )
          if $digest eq $hash;
      }
    };
  }

  # If a user is not authenticated (= if this is not an app specific endpoint,
  # or if the user does not have app-specific authorization for that app)
  # then try to authenticate with the user's global key
  unless ($self->is_user_authenticated) {
    my ($key) = $user->keys;
    if ($key) {
      my $hash = $self->app->hmac_b64('SHA256', $key->key, $query_string);
      $self->authenticate(undef, undef, { auto_validate => $user->id } )
        if $digest eq $hash;
    }
  }
}

1;
