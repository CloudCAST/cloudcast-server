package CloudCAST::Controller::Worker;

our $VERSION = '0';

use Moo;
use CloudCAST::Codes -all;

with 'CloudCAST::Base';
extends 'Mojolicious::Controller';

use Log::Any;
my $log = Log::Any->get_logger(category => 'CloudCAST');

has [qw( type name client worker )] => (
  is => 'rw',
  default => undef,
);

sub BUILD {
  my ($self, $args) = @_;

  # Keep connections with workers alive
  $self->inactivity_timeout(0);
}

sub register {
  my $self = shift->openapi->valid_input or return;

  $self->id($self->tx->req->param('id'));
  $self->worker($self);

  $log->infof('New worker available: %s', $self->id);
  $self->app->add_worker($self);
  $self->state(CLOUDCAST_CONNECTED);

  $self->on( json => sub {
    my ($ws, $msg) = @_;

    if (defined $msg and ref $msg eq 'HASH') {
      my $state = $self->state;
      if ($state < CLOUDCAST_READY) {
        if ($msg->{status} == CLOUDCAST_READY) {
          $log->infof('%s: %s is ready; notifying client', $self->id, ref $self);

          # Worker is ready
          $self->worker->state(CLOUDCAST_READY);

          # Client is ready
          $self->client->state(CLOUDCAST_READY);
          $self->client->decode_stream;
        }
        elsif ($msg->{status} == CLOUDCAST_INITIALIZED) {
          $log->infof('%s: Worker is already initialised', $self->id);
          $self->type( $msg->{type} );
          $self->name( $msg->{name} );
        }
      }

      elsif ($self->state == CLOUDCAST_READY) {
        if ($msg->{status} == CLOUDCAST_FINISHED) {
          $log->infof('%s: Worker finished procesing stream', $self->id);
          $self->client->finish(WS_FINISHED, 'Finished processing stream');
          $self->app->remove_client($self->client->id);
          $self->client(undef);
        }

        else {
          $log->debugf('%s: Forwarding message to client: %s', $self->id, $msg);
          $self->client->send({ json => $msg });
        }
      }

      else {
        $log->warnf('%s: Received message while not ready!', $self->id);
        $log->debugf("%s: Dumping message:\n%s", $self->id, $msg);
      }
    }
  });

  $self->on( finish => sub {
    my ($ws, $msg) = @_;

    $log->infof('Worker %s disconnected', $self->id);
    if ($self->client and $self->client->tx) {
      my $msg = 'Worker disconnected';
      $self->client->send({ json => {
        status => CLOUDCAST_RESOURCE_MISSING,
        message => $msg,
      }}) if $self->client->tx;
      $self->client->finish(WS_GOING_AWAY, $msg)
    }
    $self->app->remove_worker($self->id);
  });
}

1;
