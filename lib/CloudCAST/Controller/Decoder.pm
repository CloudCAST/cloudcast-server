package CloudCAST::Controller::Decoder;

our $VERSION = '0';

use Moo;
use Try::Tiny;
use Path::Tiny qw( path );
use CloudCAST::Codes -cloudcast;

with 'CloudCAST::Base';
extends 'Mojolicious::Controller';

use Log::Any;
my $log = Log::Any->get_logger(category => 'CloudCAST');

has [qw( client worker )] => (
  is => 'rw',
  default => undef,
);

sub BUILD {
  my ($self, $args) = @_;

  # Timeout for connections with clients
  $self->inactivity_timeout(60);
}

sub create {
  my $self = shift->openapi->valid_input or return;
  ...
}

sub update {
  my $self = shift->openapi->valid_input or return;
  ...
}

sub delete {
  my $self = shift->openapi->valid_input or return;
  ...
}

sub status {
  my $self = shift->openapi->valid_input or return;

  my $status = ($self->app->connected_workers() > 0) ? 'OK' : 'UNAVAILABLE';
  $log->infof('%s: Requesting server status. Server is %s', $self->id, $status);

  return $self->render(openapi => { status => $status });
}

sub init_worker {
  my ($self) = @_;

  $log->infof('Incoming client connection: %s', $self->id);
  if ($self->tx->is_websocket) {
    use Try::Tiny;
    try {
      my $decoder_id = $self->tx->req->param('decoder') // 'voxforge';
      $log->infof(q{%s: Client requests decoder '%s'}, $self->id, $decoder_id);

      my $decoder = $self->load( Decoding => $decoder_id );

      unless ($decoder) {
        return $self->finish(
          CLOUDCAST_BAD_INPUT, 'No such decoder: ' . $decoder_id
        );
      }

      $self->_get_worker({
        type => $decoder->recogniser->type,
        name => $decoder_id,
      });

      $log->infof('%s: Using worker %s', $self->id, $self->worker->id);

      $log->infof('%s: %s received init request', $self->id, ref $self);
      $self->worker->send({ json => {
        decoder => $decoder_id,
        request_id => $self->id,
        caps => $self->req->param('content-type'),
      }});
    }

    catch {
      chomp;
      $log->infof('%s: Could not initialise worker for client request: %s', $self->id, $_);

      $self->finish(
        CLOUDCAST_NOT_AVAILABLE,
        'No decoder available, try again later'
      );
    };
  }
}

sub decode_stream {
  my ($self) = @_;

  if (defined $self->tx) {
    $self->client($self);

    $self->app->add_client($self);

    $self->on(frame => sub {
      my ($ws, $frame) = @_;
      my ($opcode, $payload) = @{$frame}[4,5];

      my $state = $self->state;
      if ($state == CLOUDCAST_READY) {
        if ($opcode == 1 and $payload eq 'EOS') {
          $log->infof('%s: Recogniser received EOS. Sending to worker', $self->id);
          $self->worker->send({ text => $payload });
        }

        else {
          use bytes;
          $log->debugf('%s: Forwarding %i bytes to worker', $self->id, bytes::length $payload);
          $self->worker->send({ binary => $payload });
        }
      }

      else {
        $log->infof('%s: Recogniser (state = %s) received message with opcode %s: %.30s', $self->state, $opcode, $payload);
      }
    });
  }

  else {
    # Client disconnected before worker was ready?

    $log->infof('Client %s disconnected', $self->id);
    if (defined $self->worker) {
      $self->worker->finish;
      $self->worker->client(undef);
      $self->worker(undef);
    }

    $self->app->remove_client($self->id);
  }
}

sub _get_worker {
  my $self = shift;
  $self->worker( $self->app->get_worker(@_) )
    or die "No suitable worker in roster\n";
  $self->worker->client($self);
}

1;
