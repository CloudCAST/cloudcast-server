package CloudCAST;
# ABSTRACT: Provides data management and speech recognition services

our $VERSION = '0.001';

use Moo;
use MooX::HandlesVia;
use MooX::late;

use Types::Standard qw( Maybe InstanceOf HashRef Str );
use Types::Path::Tiny qw( File );
use File::Share qw( dist_file );
use Lingua::EN::Inflexion;
use Class::Load qw( load_class );
use Try::Tiny;

extends 'Mojolicious';
with 'CloudCAST::Base';

use Log::Any;
my $log = Log::Any->get_logger;

has csrftoken => (
  is => 'rw',
  isa => Str,
  default => sub {
    use Mojo::Util qw/md5_sum/;
    md5_sum( md5_sum( time() . {} . rand() . $$ ));
  },
);

has api_spec => (
  is => 'ro',
  isa => Maybe[File],
  coerce => 1,
);

has workers => (
  is => 'ro',
  isa => HashRef,
  handles_via => 'Hash',
  default => sub { +{} },
  handles => {
    connected_workers => 'count',
    remove_worker     => 'delete',
    list_workers      => 'keys',
    add_worker        => 'set',
    all_workers       => 'values',
  },
);

has clients => (
  is => 'ro',
  isa => HashRef,
  handles_via => 'Hash',
  default => sub { +{} },
  handles => {
    connected_clients => 'count',
    remove_client     => 'delete',
    list_clients      => 'keys',
    add_client        => 'set',
  },
);

has schema => (
  is => 'rw',
  isa => InstanceOf['CloudCAST::Schema'],
  lazy => 1,
  default => sub {
    use CloudCAST::Schema;
    CloudCAST::Schema->connect({
      dsn      => 'dbi:Pg:host=localhost',
      user     => $_[0]->config->{database}->{username},
      password => $_[0]->config->{database}->{password},
    });
  },
);

has configfile => (
  is => 'ro',
  isa => Maybe[File],
  lazy => 1,
  coerce => 1,
  default => sub { dist_file('CloudCAST', 'cloudcast.json') },
);

around [qw( add_client add_worker )] => sub {
  my $orig = shift;
  my $self = shift;

  $self->$orig($_[0]->{id} => $_[0]);
  return $_[0];
};

after [qw( add_client remove_client )] => sub {
  my $n = $_[0]->connected_clients;
  $log->debug(inflect "There <#d:$n> <V:is> <#n:$n> connected <N:client>");
};

after [qw( add_worker remove_worker )] => sub {
  my $n = $_[0]->connected_workers;
  $log->debug(inflect "There <#d:$n> <V:is> <#n:$n> connected <N:worker>");
};

sub get_worker {
  my ($self, $spec) = @_;

  return undef unless $self->connected_workers;

  my $worker;
  if ($spec) {
    # Look for matching initialised workers;
    ($worker) = grep {
      $_->name and $_->name eq $spec->{name}
    } $self->all_workers;
  }

  # If we have no spec, or couldn't find any suitable workers,
  # get any that's not initialised
  unless ($worker) {
    ($worker) = grep { !defined $_->type } $self->all_workers;
  }

  # No worker found :(
  return $worker;
}

sub startup {
  my $self = shift;

  $log->infof( 'Launched %s server', ref $self);

  # Clear default hook to install our own error handling
  $self->plugins->unsubscribe('around_dispatch');

  # Terminate session after every dispatch, so server is stateless
  $self->hook(after_dispatch => sub { shift->logout });

  $self->plugin( 'Documentation' => {
    index => 'CloudCAST',
    route => $self->routes->under('/doc'),
    template => 'perldoc',
  });

  $self->plugin( 'plack_middleware' => [
    'PrettyException' => { force_json => 1 },
  ]);

  $self->plugin( 'Minion' => { SQLite => 'jobs.db' } );

  # Load external configuration
  $self->plugin( 'JSONConfig' => file => $self->configfile );
  $self->secrets($self->config->{secrets});

  # Facilitate user authentication and session management
  $self->plugin( 'authentication' => {
    autoload_user => 1,
    session_key => 'wickedapp',
    validate_user => sub {
      my ($app, $name, $pass, $extradata) = @_;

      return $extradata->{auto_validate}
        if defined $extradata->{auto_validate};

      my $rs = $self->resultset('User');

      my $user = $rs->search( { name => $name } )->first;
      return undef unless defined $user;

      my $cost = $app->config->{authentication}->{bcrypt}->{cost};
      if ($user->validate_password($pass, $cost)) {
        return $user->id;
      }
      else {
        return undef;
      }
    },
    load_user => sub { shift->load( User => { id => shift } ) },
  });

  # Temporarily enable CORS for all
  $self->plugin( 'CORS' );

  # Access to several cryptographic methods
  $self->plugin( 'Crypto' );

  # Enable OAuth2 authentication
  $self->plugin(
    OAuth2 => $self->app->config->{authentication}->{oauth2}
  );

  if ($self->api_spec) {
    $log->infof( 'Loading API spec from %s', $self->api_spec);
    my $route = $self->routes
      ->under('/api/v0' => { 'cors.origin' => '*' } )
      ->to('user#auth');
    $self->plugin( 'OpenAPI' => {
      route => $route,
      url   => $self->api_spec,
      version_from_class => 0,
    });
  }

  # Add Link headers for queries that paginate
  $self->plugin( 'PageHeaders' );

  $self->helper( 'reply.exception' => sub {
    shift;
    $self->throw( shift, @_ );
  });

  $self->helper( 'reply.not_found' => sub {
    my $c = shift;
    $self->throw( 'NotFound' => {
      model => 'page',
      query => {
        url => $c->req->url->to_string,
      },
    });
  });

  $self->helper( 'reply.forbidden' => sub {
    my $c = shift;
    $self->throw( 'Forbidden' => {
      action => $c->openapi->spec->{'operationId'},
    });
  });

  # Replace the current_user helper from M:P:Authentication so it dies if
  # there is no user authenticated
  my $old_helper = $self->renderer->get_helper( 'current_user');
  $self->helper( 'current_user' => sub {
    my $user = $old_helper->(@_)
      or $self->throw( 'NoUserAuthenticated' => {
        action => $_[0]->openapi->spec->{'operationId'},
      });
    return $user;
  });

  # Replace the error response from M:P:OpenAPI
  $self->helper( 'openapi.valid_input' => sub {
    my $c = shift;
    my ($e) = $self->renderer->get_helper( 'openapi.validate')->($c, @_);

    if ($e) {
      $self->reply->exception( BadParams => {
        function => $c->openapi->spec->{'operationId'},
        params => { map { $_ => $e->$_ } qw( path message ) },
      });
    }

    return $c;
  });

  $self->helper( send_email => sub {
    my ($c, $address, $subject, $body) = @_;

    my $conf = $self->config->{email};

    unless ($conf) {
      $log->errorf('Cannot send email without configuration');
      return;
    }

    require Email::Simple;
    require Email::Sender::Simple;
    require Email::Sender::Transport::SMTP;

    my $email = Email::Simple->create(
      header => [
        To      => $address,
        From    => $conf->{username},
        Subject => $subject,
      ],
      body => $body,
    );

    try {
      Email::Sender::Simple->send(
        $email,
        {
          transport => Email::Sender::Transport::SMTP->new({
            host => $conf->{host},
            port => $conf->{port},
            sasl_username => $conf->{username},
            sasl_password => $conf->{password},
            ssl => 1,
          }),
        },
      );
    }
    catch {
      $log->errorf('Cloud not send email: %s', $_);
    };
  });

  $self->minion->add_task(
    email_task => sub { shift->app->send_email(@_) },
  );

  $self->helper( 'jwt.encode' => sub {
    my ($c, $jwt) = @_;

    require DateTime;
    require Mojo::JWT;

    return Mojo::JWT->new(
      claims => {
        iss => 'https://cloudcast.sheffield.ac.uk',
        iat => DateTime->now->epoch,
        %{$jwt},
      },
      secret => $self->config->{secrets},
    )->encode;
  });

  $self->helper( 'jwt.decode' => sub {
    my ($c, $jwt) = @_;

    require Mojo::JWT;

    return  Mojo::JWT->new(
      secret => $self->app->config->{secrets}
    )->decode($jwt);
  });

  $self->helper( load => sub {
    my $c = shift;
    return $c->app->_load( 0, 'NotFound', @_ );
  });

  $self->helper( does_not_exist => sub {
    my $c = shift;
    return $c->app->_load( 1, 'AlreadyExists', @_ );
  });

  $self->helper( throw => sub {
    my $c = shift;
    return $c->app->_throw( @_ );
  });

  $self->helper( resultset => sub { shift->app->schema->resultset( shift ) } );
}

sub _load {
  my ($self, $cond, $exception, $model, $search) = @_;

  $search = (ref $search eq 'HASH') ? $search : { name => $search };

  my $value = $self->schema
    ->resultset(ucfirst $model)
    ->search($search)
    ->first;

  if ( !!($value) == !!($cond) ) {
    $self->_throw( $exception => {
      model => lc($model),
      query => $search,
    });
  }

  return $value;
}

sub _throw {
  my $self = shift;
  my $exception = shift;
  my $data = shift // {};

  my $class = __PACKAGE__ . '::X';
  $class .= '::' . $exception if $exception;

  load_class $class;
  my $error = $class->new($data);
  $error->throw;
}

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST - Provides data management and speech recognition services

=head1 DESCRIPTION

This is a real-time full-duplex speech recognition server, inspired
on the Kaldi GStreamer server, but written from scratch for compatibility
with the CloudCAST architecture.

This particular prototype is implemented in Mojolicious and relies on
the GStreamer framework for media stream handling, and Kaldi for the
automatic recognition.

=head2 What is different from the original?

In the original server, the worker process which connected to the server
was initialised with an instance of a decoder class. Upon construction,
the worker created a GStreamer pipeline with the appropriate decoder, and
started working only after this was complete.

This allowed the worker to I<always> be ready to process audio using that
pipeline, since the lifetimes of the pipeline and that of the worker itself
were tied.

This server decouples these two processes to allow for workers who are in
a way "decoder-agnostic". Workers can be created and connected to the server
already initialised; or left in a "raw" state, waiting for initialisation.

When the server receives an incoming connection, it forwards the init request
to the worker. This request includes information about the decoder that is
needed, including some general parameters, the name of the GStreamer element
factory to use, and any additional parameters passed to that factory.

Once the requested pipeline has been created and is ready to start receiving
data, the worker sends its ready state to the server, which forwards it to the
client. The client is then free to start sending data.

When the connection to the client is done, the worker may destroy the decoder
to go back to a "raw" state, or it may choose to remain in an initialised mode,
in which case it should receive only those connections that match the existing
decoder.

When workers connect to the server, they should therefore report two pieces of
information:

=over 4

=item 1 Whether they are initialised

=item 2 The decoder types that are supported

=back

A worker as used in the original Kaldi GStreamer server would report being
initialised, and supporting only a single decoder type: the one currently
attached.

Ideally, the server should keep a track of the workers that are connected in
a directory that kept data on

=over 4

=item * Which workers are initialised

=item * What decoders are they linked to

=item * What decoders are supported by each worker

=back

The implementation in the current prototype does not go this far. Currently,
all workers are assumed by the server to be "raw", and to support the
C<onlinegmmdecodefaster> and C<kaldinnet2onlinedecoder> decoders.

This approach has a cost in initialisation time, since loading larger
recognition models in memory can take some time. But since workers can in
principle already be initialised, it adds a great deal of versatility.
Specially since this also means that the connection between the server and
Kaldi is circumstancial: if a different GStreamer plugin connecting to eg.
HTK (or whatever other framework) is available, then in principle that should
be usable as well.

=head1 INSTALLATION

The CloudCAST server has been packaged following the standards of any other
Perl distribution, so it should be installable using any of the Perl standards
tools.

=head2 Stable releases

Formal releases of the server exist on the C<release> branch, where each commit
should point to a stable release. While on that branch, running

    # On the 'release' or 'build' branch
    cpanm .

should be enough to install the server.

=head2 Non-stable builds

Non-stable releases, built from development files, exist on the C<build>
branch. Sources on this branch are not necessarily release-ready, but should
I<also> be installable using the traditional methods.

=head2 Development files

Particularly while under heavy development, the C<master> branch holds
development source files. These I<may> be ready for installation, but they'll
need to be turned into a compliant distribution before using the above
mentioned methods will work.

Luckily, doing so is easy using L<Dist::Zilla>:

    # If on the 'master' branch
    dzil authordeps --missing | cpanm
    dzil listdeps   --missing | cpanm
    dzil build

    # Now change to the 'build' branch, or `cd` into CloudCAST-*
    # and install as above

=head2 Requirements

Other than the Perl requirements, which should be handled automatically by the
installation process above, the server requires some additional configuration
on the host machine.

=head3 Kaldi

Download and compile L<Kaldi|http://kaldi.sourceforge.net>.

Also compile the online extensions (C<make ext>) and the Kaldi GStreamer plugin
(see C<README> in Kaldi's C<src/gst-plugin> directory).

The server looks for Kaldi either by reading the C<KALDI_ROOT> environment
variable, or by looking in C<external/kaldi> under the distribution's root directory.

=head3 GStreamer

Download and compile L<Kaldi|http://kaldi.sourceforge.net>.

Also compile the online extensions (C<make ext>) and the Kaldi GStreamer plugin
(see C<README> in Kaldi's C<src/gst-plugin> directory).

Any GStreamer plugin that is available on the host system should be also
available for the server. Make sure the C<GST_PLUGIN_PATH> environment
variable points to where these should be found. If this is unset, plugins will
be searched in C<src/gst-plugin> under whatever is set as the Kaldi root.

In particular, you might be interested L<in the Kaldi NNET2 online decoder
plugin|https://github.com/alumae/gst-kaldi-nnet2-online> by Tanel Alumae

Note that in order to use the Perl modules interacting with GStreamer, you
might have to install a certain number of OS-specific packages. On a
Debian / Ubuntu machine, installing these should do (but please check your
distribution's documentation):

=over 4

=item * libgirepository1.0-dev

=item * libgstreamer1.0-dev

=item * gir1.2-gstreamer-1.0

=item * gir1.2-gst-plugins-base-1.0

=back

=head3 Database

Current versions of the server have only been tested using PostgreSQL. Please
follow your operating system's instructions for how to install it.

=head3 Acoustic and language models for Kaldi

You need GMM-HMM or DNN-based ("online2") acoustic models. The models provided
in the distribution of the original server should work with this server as well.

=head3 Decoder configuration

Unlike the original server, decoders have been abstracted into a class of their
own (see L<CloudCAST::Schema::Result::Decoding>). These are generally similar
to those created with the configuration files in the original server, and in
a bind, instantiating a decoder with the parameters from those files should
work.

For a more tailored use of the CloudCAST classes, separate the parameters in
those configuration files into the appropriate classes:

=over 4

=item * CloudCAST::Schema::Result::Decoding

=over 4

=item * CloudCAST::Schema::Result::Recogniser

=over 4

=item * CloudCAST::Schema::Result::Grammar

=item * CloudCAST::Schema::Result::Lexicon

=item * CloudCAST::Schema::Result::RecognitionModel

=back

=back

=back

For an example of the existing decoders, please see the database population
scripts under the L<CloudCAST::Schema> distribution.

=head1 RUNNING THE SERVER

Either of the following starts the main server on localhost:3000

    morbo bin/cloudcast-server
    bin/cloudcast-server daemon -l http://[::]:3000

This can be further configured using eg. C<supervisord>, as with the sample
configuration file in the C<examples> directory.

=head3 Running workers

The master server doesn't perform speech recognition itself; it simply
delegates client recognition requests to workers. You need one worker per
recognition session. The number of running workers should thus be at least the
number of potential concurrent recognition sessions.

It should be noted that workers are fully independent and do not even have to
be running on the same machine, thus offering practically unlimited
parallelness.

For more information, see the L<CloudCAST::Worker> distribution, and the sample
worker script in C<bin/cloudcast-worker> (in that distribution).

=head1 SERVER USAGE

A sample implementation of the streaming client is in C<bin/cloudcast-client>.

If the server/worker has been started as described above, you should be able
to test the installation by invoking:

    bin/cloudcast-client data/test/english.wav

And receive the following output

    THE. ONE TWO THREE FOUR FIVE SIX SEVEN EIGHT.

If you run the client by specifying the DNN-based online models (with eg.
C<--decoder tedlium>), you should see something like this:

    one two or three you fall five six seven eight. yeah.

=head2 The HTTP API

One can also use the server through a very simple HTTP-based API.
This allows to simply send audio via a PUT or POST request to
C<http://server:port/user/foo/app/bar/asr/decode> and read the JSON ouput.

A full OpenAPI schema of the API is under development at
L<https://gitlab.com/CloudCAST/spec>. You can see an automatically generated
interactive version of the documentation at
L<http://docs.cloudcast.apiary.io/>.

=head2 WebSocket-based client-server protocol

=head3 Opening a session

To open a session, connect to the specified server websocket address
(e.g. C<ws://localhost:3000/user/foo/app/bar/asr/decode>). The server assumes
by default that incoming audio is sent using 16 kHz, mono, 16bit little-endian
format. This can be overriden using the C<content-type> request parameter.
The content type has to be specified using GStreamer 1.0 caps format,
e.g. to send 44100 Hz mono 16-bit data, use C<audio/x-raw, layout=(string)interleaved, rate=(int)44100, format=(string)S16LE, channels=(int)1>

This will need to be url-encoded, so the actual request would look like the
following (with line breaks added for readability):

    ws://localhost:3000/user/foo/app/bar/asr/decode
      ?content-type=audio/x-raw,
        +layout=(string)interleaved,
        +rate=(int)44100,
        +format=(string)S16LE,
        +channels=(int)1

Audio can also be encoded using any codec recognized by GStreamer (assuming the
needed packages are installed on the server). GStreamer should recognize the
container and codec automatically from the stream, so you shouldn't have to
specify the content type. So you should be able to send e.g. audio encoded
using the Speex codec in an Ogg container and the server should automatically
recognize the codec.

This has not been fully tested, but since it is part of the documented
behaviour for GStreamer, it should Just Work.

=head3 Establishing the connection

Once the server receives the WebSocket request, it will determine what needs to
be done to correctly decode that stream of data. In particular, this means
determining what decoder pipeline is needed, and whether there are any running
workers that have either already loaded that pipeline, or support it (and are
available).

To determine this, all parameters passed as part of the original request will
be passed to the worker for initialisation (see L<CloudCAST::Worker> for more
details).

Applications can have different default decoding parameters. Please check the
documentation for L<CloudCAST::Schema::Result::Application> for more details.

Once this initialisation process has been completed, and a worker is ready to
process the incoming stream, the connection with the client will be upgraded to
a WebSocket connection, and the client is free to start sending data, and the
connection between client and server behaves like that of the original
server.

If any problems arise during this time, the client will receive an error
response.

=head3 Sending audio

Speech should be sent to the server in raw blocks of data, using the encoding
specified when session was opened. It is recommended that a new block is sent
at least 4 times per second (less frequent blocks would increase the
recognition lag). Blocks do not have to be of equal size.

After the last block of speech data, a special 3-byte ANSI-encoded string
C<EOS> ("end-of-stream") needs to be sent to the server. This tells the
server that no more speech is coming and the recognition can be finalized.
Please note that this signal can also be automatically determined by the server
after periods of prolongued silence.

After sending "EOS", client should keep the WebSocket connection open to
receive any remaining recognition results from the server. The server will
closes the connection itself when all recognition results have been sent to the
client.

Sending additional audio data after the C<EOS> signal has been sent is an error
and will result in the immediate termination of the connection. In order to
process a new audio stream, a new websocket connection has to be created by
the client.

=head3 Reading results

The server sends recognition results and other information to the client as
JSON. Responses can contain the following fields:

    * status           response status (integer), see codes below
    * message          (optional) status message
    * result           (optional) a list of recognition results
      - final          true when the hypothesis is final
      - hypotheses     a list with each item containing the following:
        + transcript   recognized words
        + confidence   (optional) confidence of the hypothesis (float, 0..1)

For a list of status codes and their meanings, please see L<CloudCAST::Codes>.

Examples of server responses:

    # An error response when workers are not available
    {"status": 4005}

    # A partial result with a single hypothesis
    {
      "status": 4000,
      "result": {
        "final": false
        "hypotheses": [
          {"transcript": "see on"}
        ],
      }
    }

    # A final result with a single hypothesis
    {
      "status": 4000,
      "result": {
        "final": true
        "hypotheses": [
          {"transcript": "see on teine lause."}
        ],
      }
    }

The server will segment incoming audio on the fly. For each segment, many
non-final hypotheses, followed by one final hypothesis are sent. Non-final
hypotheses are used to present partial recognition hypotheses to the client.
A sequence of non-final hypotheses is always followed by a final hypothesis
for that segment.

After sending a final hypothesis for a segment, the server will either start
decoding the next segment, or close the connection if all audio sent by the
client has been processed already.

The client is reponsible for presenting these results to the user in a way
suitable for the application.

=head3 Client software

A JavaScript client is available at L<http://gitlab.com/cloudcast/cloudcast.js>

=head1 SEE ALSO

=over 4

=item L<CloudCAST::Codes>

=item L<CloudCAST::Schema>

=item L<CloudCAST::Worker>

=item L<https://docs.apiary.io/cloudcast>

=item L<http://cloudcast.sheffield.ac.uk>

=back

=head1 ACKNOWLEDGEMENTS

Work on this server has been funded by the Leverhulme Trust as part of the
CloudCAST project.

This server is heavily based in the Kaldi GStreamer server and other work by
Tanel Alumae.

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2016-2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
