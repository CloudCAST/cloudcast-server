# NAME

CloudCAST - Provides data management and speech recognition services

# DESCRIPTION

This is a real-time full-duplex speech recognition server, inspired
on the Kaldi GStreamer server, but written from scratch for compatibility
with the CloudCAST architecture.

This particular prototype is implemented in Mojolicious and relies on
the GStreamer framework for media stream handling, and Kaldi for the
automatic recognition.

## What is different from the original?

In the original server, the worker process which connected to the server
was initialised with an instance of a decoder class. Upon construction,
the worker created a GStreamer pipeline with the appropriate decoder, and
started working only after this was complete.

This allowed the worker to _always_ be ready to process audio using that
pipeline, since the lifetimes of the pipeline and that of the worker itself
were tied.

This server decouples these two processes to allow for workers who are in
a way "decoder-agnostic". Workers can be created and connected to the server
already initialised; or left in a "raw" state, waiting for initialisation.

When the server receives an incoming connection, it forwards the init request
to the worker. This request includes information about the decoder that is
needed, including some general parameters, the name of the GStreamer element
factory to use, and any additional parameters passed to that factory.

Once the requested pipeline has been created and is ready to start receiving
data, the worker sends its ready state to the server, which forwards it to the
client. The client is then free to start sending data.

When the connection to the client is done, the worker may destroy the decoder
to go back to a "raw" state, or it may choose to remain in an initialised mode,
in which case it should receive only those connections that match the existing
decoder.

When workers connect to the server, they should therefore report two pieces of
information:

- 1 Whether they are initialised
- 2 The decoder types that are supported

A worker as used in the original Kaldi GStreamer server would report being
initialised, and supporting only a single decoder type: the one currently
attached.

Ideally, the server should keep a track of the workers that are connected in
a directory that kept data on

- Which workers are initialised
- What decoders are they linked to
- What decoders are supported by each worker

The implementation in the current prototype does not go this far. Currently,
all workers are assumed by the server to be "raw", and to support the
`onlinegmmdecodefaster` and `kaldinnet2onlinedecoder` decoders.

This approach has a cost in initialisation time, since loading larger
recognition models in memory can take some time. But since workers can in
principle already be initialised, it adds a great deal of versatility.
Specially since this also means that the connection between the server and
Kaldi is circumstancial: if a different GStreamer plugin connecting to eg.
HTK (or whatever other framework) is available, then in principle that should
be usable as well.

# INSTALLATION

The CloudCAST server has been packaged following the standards of any other
Perl distribution, so it should be installable using any of the Perl standards
tools.

## Stable releases

Formal releases of the server exist on the `release` branch, where each commit
should point to a stable release. While on that branch, running

    # On the 'release' or 'build' branch
    cpanm .

should be enough to install the server.

## Non-stable builds

Non-stable releases, built from development files, exist on the `build`
branch. Sources on this branch are not necessarily release-ready, but should
_also_ be installable using the traditional methods.

## Development files

Particularly while under heavy development, the `master` branch holds
development source files. These _may_ be ready for installation, but they'll
need to be turned into a compliant distribution before using the above
mentioned methods will work.

Luckily, doing so is easy using [Dist::Zilla](https://metacpan.org/pod/Dist::Zilla):

    # If on the 'master' branch
    dzil authordeps --missing | cpanm
    dzil listdeps   --missing | cpanm
    dzil build

    # Now change to the 'build' branch, or `cd` into CloudCAST-*
    # and install as above

## Requirements

Other than the Perl requirements, which should be handled automatically by the
installation process above, the server requires some additional configuration
on the host machine.

### Kaldi

Download and compile [Kaldi](http://kaldi.sourceforge.net).

Also compile the online extensions (`make ext`) and the Kaldi GStreamer plugin
(see `README` in Kaldi's `src/gst-plugin` directory).

The server looks for Kaldi either by reading the `KALDI_ROOT` environment
variable, or by looking in `external/kaldi` under the distribution's root directory.

### GStreamer

Download and compile [Kaldi](http://kaldi.sourceforge.net).

Also compile the online extensions (`make ext`) and the Kaldi GStreamer plugin
(see `README` in Kaldi's `src/gst-plugin` directory).

Any GStreamer plugin that is available on the host system should be also
available for the server. Make sure the `GST_PLUGIN_PATH` environment
variable points to where these should be found. If this is unset, plugins will
be searched in `src/gst-plugin` under whatever is set as the Kaldi root.

In particular, you might be interested [in the Kaldi NNET2 online decoder
plugin](https://github.com/alumae/gst-kaldi-nnet2-online) by Tanel Alumae

Note that in order to use the Perl modules interacting with GStreamer, you
might have to install a certain number of OS-specific packages. On a
Debian / Ubuntu machine, installing these should do (but please check your
distribution's documentation):

- libgirepository1.0-dev
- libgstreamer1.0-dev
- gir1.2-gstreamer-1.0
- gir1.2-gst-plugins-base-1.0

### Database

Current versions of the server have only been tested using PostgreSQL. Please
follow your operating system's instructions for how to install it.

### Acoustic and language models for Kaldi

You need GMM-HMM or DNN-based ("online2") acoustic models. The models provided
in the distribution of the original server should work with this server as well.

### Decoder configuration

Unlike the original server, decoders have been abstracted into a class of their
own (see [CloudCAST::Schema::Result::Decoding](https://metacpan.org/pod/CloudCAST::Schema::Result::Decoding)). These are generally similar
to those created with the configuration files in the original server, and in
a bind, instantiating a decoder with the parameters from those files should
work.

For a more tailored use of the CloudCAST classes, separate the parameters in
those configuration files into the appropriate classes:

- CloudCAST::Schema::Result::Decoding
    - CloudCAST::Schema::Result::Recogniser
        - CloudCAST::Schema::Result::Grammar
        - CloudCAST::Schema::Result::Lexicon
        - CloudCAST::Schema::Result::RecognitionModel

For an example of the existing decoders, please see the database population
scripts under the [CloudCAST::Schema](https://metacpan.org/pod/CloudCAST::Schema) distribution.

# RUNNING THE SERVER

Either of the following starts the main server on localhost:3000

    morbo bin/cloudcast-server
    bin/cloudcast-server daemon -l http://[::]:3000

This can be further configured using eg. `supervisord`, as with the sample
configuration file in the `examples` directory.

### Running workers

The master server doesn't perform speech recognition itself; it simply
delegates client recognition requests to workers. You need one worker per
recognition session. The number of running workers should thus be at least the
number of potential concurrent recognition sessions.

It should be noted that workers are fully independent and do not even have to
be running on the same machine, thus offering practically unlimited
parallelness.

For more information, see the [CloudCAST::Worker](https://metacpan.org/pod/CloudCAST::Worker) distribution, and the sample
worker script in `bin/cloudcast-worker` (in that distribution).

# SERVER USAGE

A sample implementation of the streaming client is in `bin/cloudcast-client`.

If the server/worker has been started as described above, you should be able
to test the installation by invoking:

    bin/cloudcast-client data/test/english.wav

And receive the following output

    THE. ONE TWO THREE FOUR FIVE SIX SEVEN EIGHT.

If you run the client by specifying the DNN-based online models (with eg.
`--decoder tedlium`), you should see something like this:

    one two or three you fall five six seven eight. yeah.

## The HTTP API

One can also use the server through a very simple HTTP-based API.
This allows to simply send audio via a PUT or POST request to
`http://server:port/user/foo/app/bar/asr/decode` and read the JSON ouput.

A full OpenAPI schema of the API is under development at
[https://gitlab.com/CloudCAST/spec](https://gitlab.com/CloudCAST/spec). You can see an automatically generated
interactive version of the documentation at
[http://docs.cloudcast.apiary.io/](http://docs.cloudcast.apiary.io/).

## WebSocket-based client-server protocol

### Opening a session

To open a session, connect to the specified server websocket address
(e.g. `ws://localhost:3000/user/foo/app/bar/asr/decode`). The server assumes
by default that incoming audio is sent using 16 kHz, mono, 16bit little-endian
format. This can be overriden using the `content-type` request parameter.
The content type has to be specified using GStreamer 1.0 caps format,
e.g. to send 44100 Hz mono 16-bit data, use `audio/x-raw, layout=(string)interleaved, rate=(int)44100, format=(string)S16LE, channels=(int)1`

This will need to be url-encoded, so the actual request would look like the
following (with line breaks added for readability):

    ws://localhost:3000/user/foo/app/bar/asr/decode
      ?content-type=audio/x-raw,
        +layout=(string)interleaved,
        +rate=(int)44100,
        +format=(string)S16LE,
        +channels=(int)1

Audio can also be encoded using any codec recognized by GStreamer (assuming the
needed packages are installed on the server). GStreamer should recognize the
container and codec automatically from the stream, so you shouldn't have to
specify the content type. So you should be able to send e.g. audio encoded
using the Speex codec in an Ogg container and the server should automatically
recognize the codec.

This has not been fully tested, but since it is part of the documented
behaviour for GStreamer, it should Just Work.

### Establishing the connection

Once the server receives the WebSocket request, it will determine what needs to
be done to correctly decode that stream of data. In particular, this means
determining what decoder pipeline is needed, and whether there are any running
workers that have either already loaded that pipeline, or support it (and are
available).

To determine this, all parameters passed as part of the original request will
be passed to the worker for initialisation (see [CloudCAST::Worker](https://metacpan.org/pod/CloudCAST::Worker) for more
details).

Applications can have different default decoding parameters. Please check the
documentation for [CloudCAST::Schema::Result::Application](https://metacpan.org/pod/CloudCAST::Schema::Result::Application) for more details.

Once this initialisation process has been completed, and a worker is ready to
process the incoming stream, the connection with the client will be upgraded to
a WebSocket connection, and the client is free to start sending data, and the
connection between client and server behaves like that of the original
server.

If any problems arise during this time, the client will receive an error
response.

### Sending audio

Speech should be sent to the server in raw blocks of data, using the encoding
specified when session was opened. It is recommended that a new block is sent
at least 4 times per second (less frequent blocks would increase the
recognition lag). Blocks do not have to be of equal size.

After the last block of speech data, a special 3-byte ANSI-encoded string
`EOS` ("end-of-stream") needs to be sent to the server. This tells the
server that no more speech is coming and the recognition can be finalized.
Please note that this signal can also be automatically determined by the server
after periods of prolongued silence.

After sending "EOS", client should keep the WebSocket connection open to
receive any remaining recognition results from the server. The server will
closes the connection itself when all recognition results have been sent to the
client.

Sending additional audio data after the `EOS` signal has been sent is an error
and will result in the immediate termination of the connection. In order to
process a new audio stream, a new websocket connection has to be created by
the client.

### Reading results

The server sends recognition results and other information to the client as
JSON. Responses can contain the following fields:

    * status           response status (integer), see codes below
    * message          (optional) status message
    * result           (optional) a list of recognition results
      - final          true when the hypothesis is final
      - hypotheses     a list with each item containing the following:
        + transcript   recognized words
        + confidence   (optional) confidence of the hypothesis (float, 0..1)

For a list of status codes and their meanings, please see [CloudCAST::Codes](https://metacpan.org/pod/CloudCAST::Codes).

Examples of server responses:

    # An error response when workers are not available
    {"status": 4005}

    # A partial result with a single hypothesis
    {
      "status": 4000,
      "result": {
        "final": false
        "hypotheses": [
          {"transcript": "see on"}
        ],
      }
    }

    # A final result with a single hypothesis
    {
      "status": 4000,
      "result": {
        "final": true
        "hypotheses": [
          {"transcript": "see on teine lause."}
        ],
      }
    }

The server will segment incoming audio on the fly. For each segment, many
non-final hypotheses, followed by one final hypothesis are sent. Non-final
hypotheses are used to present partial recognition hypotheses to the client.
A sequence of non-final hypotheses is always followed by a final hypothesis
for that segment.

After sending a final hypothesis for a segment, the server will either start
decoding the next segment, or close the connection if all audio sent by the
client has been processed already.

The client is reponsible for presenting these results to the user in a way
suitable for the application.

### Client software

A JavaScript client is available at [http://gitlab.com/cloudcast/cloudcast.js](http://gitlab.com/cloudcast/cloudcast.js)

# SEE ALSO

- [CloudCAST::Codes](https://metacpan.org/pod/CloudCAST::Codes)
- [CloudCAST::Schema](https://metacpan.org/pod/CloudCAST::Schema)
- [CloudCAST::Worker](https://metacpan.org/pod/CloudCAST::Worker)
- [https://docs.apiary.io/cloudcast](https://docs.apiary.io/cloudcast)
- [http://cloudcast.sheffield.ac.uk](http://cloudcast.sheffield.ac.uk)

# ACKNOWLEDGEMENTS

Work on this server has been funded by the Leverhulme Trust as part of the
CloudCAST project.

This server is heavily based in the Kaldi GStreamer server and other work by
Tanel Alumae.

# AUTHOR

- José Joaquín Atria <jjatria@cpan.org>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2016-2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
