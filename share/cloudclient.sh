HOST="https://cloudcast.sheffield.ac.uk"
ROOT="/api/v0"
APIKEY=$(cat /etc/cloudcast/cloudcast.json | jq --raw-output .database.root_key)
VERBOSE=
TOKEN=
FORMATTER="jq"

while getopts "vRh:r:u:k:t:" opt; do
    case "$opt" in
    v)  VERBOSE=" -v "
        ;;
    h)  HOST=$OPTARG
        ;;
    r)  ROOT=$OPTARG
        ;;
    R)  FORMATTER="tee"
        ;;
    u)  USER=$OPTARG
        ;;
    k)  APIKEY=$OPTARG
        ;;
    t)  TOKEN=$OPTARG
        ;;
    *)  exit
        ;;
    esac
done

shift $((OPTIND-1))

ACTION=$1
ENDPOINT=$2
DATA=$3
AUTHORIZATION=

if [ -z "$VERBOSE" ]; then
  exec 2>/dev/null
fi

if [ -z "$TOKEN" ]; then
  TIMESTAMP=$(date "+%a, %d %b %Y %T %Z")
  DIGEST=$(echo -n "$ACTION+$ROOT$ENDPOINT+$TIMESTAMP" |
    openssl dgst -sha256 -hmac "$APIKEY" -binary |
    base64)
  AUTHORIZATION="hmac $USER:$DIGEST"
else
  AUTHORIZATION="Bearer $TOKEN"
fi

if [ -z "$DATA" ]; then
  curl $VERBOSE --request "$ACTION" \
    --header "Date: $TIMESTAMP" \
    --header "Authorization: $AUTHORIZATION" \
    "$HOST$ROOT$ENDPOINT" | "$FORMATTER"
else
  curl $VERBOSE --request "$ACTION" \
    --header "Date: $TIMESTAMP" \
    --header "Authorization: $AUTHORIZATION" \
    --data "$DATA" \
    "$HOST$ROOT$ENDPOINT" | "$FORMATTER"
fi
