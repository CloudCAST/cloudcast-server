use Mojo::Base -strict;

use Test::More;
use Test::Mojo;
use Mojo::Date;
use File::Share qw( dist_file );
use CloudCAST;
use CloudCAST::Schema;
use JSON::MaybeXS qw( decode_json );
use DDP;

my $schema = CloudCAST::Schema->connect( 'dbi:SQLite:dbname=:memory:' );
$schema->deploy;

my $root = '/api/v0';

my $t = Test::Mojo->new(
  CloudCAST->new({
    api_spec => dist_file( 'CloudCAST', 'spec/api.yaml' ),
    api_spec => 'share/spec/api.yaml',
    schema => $schema,
  })
);


# No users to start with
$t->get_ok($root . '/users')->status_is(200)->json_is([]);

# Creating users needs name and email
$t->post_ok($root . '/users')
  ->status_is(400)
  ->json_is('/ident' => 'bad_params')
  ->json_like('/params/message' => qr{expected object}i );

$t->post_ok($root . '/users' => json => { name => 'foo' })
  ->status_is(400)
  ->json_is('/ident' => 'bad_params')
  ->json_like('/params/message' => qr{missing property}i );

# Create a user
my $user = {
  name => 'foo',
  email_address => 'a@b.c',
  password => 'foo',
};

$t->post_ok($root . '/users' => json => $user )
  ->status_is(201)
  ->json_is('/name' => $user->{name})
  ->json_is('/email_address' => $user->{email_address} );

# New user is in user list
$t->get_ok($root . '/users')
  ->status_is(200)
  ->json_is('/0/name', $user->{name});

# Login as user
my $token = decode_json(
  $t->post_ok($root . '/login', json => {
    name => $user->{name}, password => $user->{password}
  })
  ->status_is(200)
  ->json_like('/token', qr{^[a-zA-Z0-9_.-]+$})
  ->tx->res->body
);
$token = $token->{token};

# Get user by name
$t->get_ok($root . '/user/' . $user->{name})
  ->status_is(200)
  ->json_is('/name', $user->{name});

# Cannot delete unless authenticated
$t->delete_ok($root . '/user/' . $user->{name})
  ->status_is(403)
  ->json_is('/ident' => 'no_user_authenticated');

# Can delete authenticated with bearer token
$t->delete_ok(
    $root . '/user/' . $user->{name},
    { Authorization => "Bearer $token" }
  )
  ->status_is(204);

# Get user by name
$t->get_ok($root . '/user/' . $user->{name})
  ->status_is(404);

# Re-create user
$t->post_ok($root . '/users' => json => $user )
  ->status_is(201);

# Edit user
$t->put_ok(
    $root . '/user/' . $user->{name},
    json => { first_name => 'Foo', last_name => 'Bar' }
  )
  ->status_is(200)
  ->json_is('/first_name', 'Foo')
  ->json_is('/last_name', 'Bar');

# Create a global key
my $key = decode_json(
  $t->post_ok(
      $root . '/user/' . $user->{name} . '/key',
      { Authorization => "Bearer $token" }
    )
    ->status_is(201)
    ->tx->res->body
);
$key = $key->{key};

# Can delete authenticated with HMAC digest
{
  my $date = Mojo::Date->new;
  # Query parameters must be sorted
  my $query = sprintf "DELETE+%s/user/%s+%s", $root, $user->{name}, $date;
  my $sign = $t->app->hmac_b64('SHA256', $key, $query);
  $t->delete_ok(
      $root . '/user/' . $user->{name},
      {
        Date => "$date",
        Authorization => sprintf("hmac %s:%s", $user->{name}, $sign),
      }
    )
    ->status_is(204)->tx->res->body;
}

done_testing();
